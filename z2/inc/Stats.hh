#ifndef STATS_HH
#define STATS_HH


class Stats{
	private:
		int iloscPyt;
		int iloscPopOdp;
	public:	
	int get_ilPyt() const {return iloscPyt;}
	int get_ilOdp() const {return iloscPopOdp;}
	void dodajPyt() {iloscPyt++;}
	void dodajPopOdp(){iloscPopOdp++;}
	double procent();
	Stats ();
};

#endif
