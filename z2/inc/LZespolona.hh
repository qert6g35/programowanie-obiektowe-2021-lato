#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH

#include <iostream>

class LZespolona{//private
private: //protected
	double re;
	double im;
public:
	double modul() const ;
	LZespolona sprzezenie() const;
	LZespolona  operator + (const  LZespolona  Skl2) const;
	LZespolona  operator - ( LZespolona  Skl2) const;
	LZespolona  operator * ( LZespolona  Skl2) const;
	LZespolona  operator / ( double  Skl2) const;
	LZespolona  operator / ( LZespolona  Skl2) const;
	double operator == ( LZespolona skl2) const;
	double operator != ( LZespolona skl2) const;
	double get_re() const {return re;}
	double get_im() const {return im;}
	void set_re(double _re) {re = _re;}
	void set_im(double _im) {im = _im;}
	LZespolona(double re , double im);
	LZespolona();
	LZespolona(double re );
};

std::istream & operator >> (std::istream & strm, LZespolona & L);
std::ostream & operator << (std::ostream & strm, const LZespolona & L);

#endif
