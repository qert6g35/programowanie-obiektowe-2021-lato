#ifndef BAZATESTU_HH
#define BAZATESTU_HH


#include "WyrazenieZesp.hh"
#include <iostream>
#include <cstring>
#include <cassert>
#include <fstream>


class BazaTestu {
private:
	std::fstream strm_pliku_pytan;

	bool poprawnosc;
public:
	
	bool pobierz_pytanie(WyrZesp & W);
	bool otworz_plik(std::string nazwa);
	BazaTestu(std::string nazwa);
	bool CzyZlapanoPlik() {return poprawnosc;}
	bool CzyKoniecPlik() {return strm_pliku_pytan.eof();}
	void zakoncz_strm();
};	
#endif
