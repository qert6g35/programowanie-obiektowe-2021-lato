#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH



#include "LZespolona.hh"
#include <iostream>

enum Operator { Op_plus, Op_minus, Op_razy, Op_dziel };


class WyrZesp {
private:
  LZespolona   L1;  
  Operator     Op;   
  LZespolona   L2;
 public:
  LZespolona get_L1() const {return L1;}
  LZespolona get_L2() const {return L2;}
  Operator   get_Op() const {return Op;}
  void set_L1(LZespolona argL1) {L1 = argL1;}
  void set_L2(LZespolona argL2) {L2 = argL2;}
  void   set_Op(Operator argop) {Op = argop;}
  LZespolona Oblicz();
  WyrZesp();
};


std::istream & operator >> (std::istream & strm, WyrZesp & W);
std::ostream & operator << (std::ostream & strm, const WyrZesp & W);

#endif
