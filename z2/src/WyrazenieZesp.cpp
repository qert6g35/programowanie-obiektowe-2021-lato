#include "WyrazenieZesp.hh"

WyrZesp::WyrZesp():L1(1.0,0.0), Op(Op_plus) , L2(0.0,1.0) {}

LZespolona WyrZesp::Oblicz(){
	switch ( Op ){
		case Op_plus:
			return (L1 + L2);
		break;
		case Op_minus:
			return (L1 - L2);
		break;
		case Op_razy:
			return (L1 * L2);
		break;
		case Op_dziel:
			return (L1 / L2);
		break;
	}
}


std::istream & operator >> (std::istream & strm, WyrZesp & W){ //funkcja wczytaj
  char znak;
  LZespolona a ,b;
	strm >> a >> znak >> b;
	//std::cout << std::endl << a << znak << b << std::endl;
	switch (znak){
		case '+':
			W.set_L1(a);
			W.set_L2(b);
			W.set_Op(Op_plus);
		break;
		case '-':
			W.set_L1(a);
			W.set_L2(b);
			W.set_Op(Op_minus);
		break;
		case '*':
			W.set_L1(a);
			W.set_L2(b);
			W.set_Op(Op_razy);
		break;
		case '/':
			W.set_L1(a);
			W.set_L2(b);
			W.set_Op(Op_dziel);
		break;
		default:
			strm.setstate(std::ios::failbit);
			return strm;
		break;
	}
	return strm;
}

std::ostream & operator << (std::ostream & strm, const WyrZesp & W){ //funkcja wyświetl
	char znak;
	switch (W.get_Op()){
		case Op_plus:
			znak = '+';
		break;
		case Op_minus:
			znak = '-';
		break;
		case Op_razy:
			znak = '*';
		break;
		case Op_dziel:
			znak = '/';
		break;
	}
		
	std::cout << W.get_L1() << " " << znak << " " << W.get_L2();

	return strm;
}



