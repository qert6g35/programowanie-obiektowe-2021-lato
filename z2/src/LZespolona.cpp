#include "LZespolona.hh"

	LZespolona::LZespolona(double argre , double argim): re(argre), im(argim) {}
	
	LZespolona::LZespolona(double argre): re(argre), im(0.0) {}
	
	LZespolona::LZespolona(): re(0.0), im(0.0) {}


double LZespolona::modul() const {
	return (re * re + im * im);
}

LZespolona LZespolona::sprzezenie()const{
	LZespolona zesp;
	zesp.set_re(re);
	zesp.set_im(-im);
	return zesp;
}

LZespolona  LZespolona::operator + (  LZespolona  Skl2) const{
  LZespolona  Wynik;

  Wynik.set_re ( re + Skl2.get_re());
  Wynik.set_im ( im + Skl2.get_im());
  return Wynik;
}

LZespolona  LZespolona::operator - ( LZespolona  Skl2 ) const{
  LZespolona  Wynik;

  Wynik.set_re ( re - Skl2.get_re());
  Wynik.set_im ( im - Skl2.get_im());
  return Wynik;
}

LZespolona  LZespolona::operator * ( LZespolona  Skl2 ) const{
  LZespolona  Wynik;

  Wynik.set_re ( re * Skl2.get_re() - im * Skl2.get_im());
  Wynik.set_im ( re * Skl2.get_im() + im * Skl2.get_re());
  return Wynik;
}

LZespolona  LZespolona::operator / ( double  Skl2) const{
  LZespolona  Wynik;

  Wynik.set_re ( re / Skl2 );
  Wynik.set_im ( im / Skl2 );
  return Wynik;
}

LZespolona  LZespolona::operator / (LZespolona  Skl2) const{
  LZespolona  Wynik ;
  LZespolona Skl1(re,im);
	LZespolona sprz = Skl2.sprzezenie();
	double mod2 = Skl2.modul();	
	Wynik = (Skl1 * sprz) / mod2;
  return Wynik;
}

double LZespolona::operator == (LZespolona skl2) const{
	if((abs(re - skl2.get_re()) < 0.0000001)&(abs(im - skl2.get_im()) < 0.0000001)) return true;
	return false; 
}

double LZespolona::operator != (LZespolona skl2) const{
	LZespolona skl1(re,im);
	if(skl1 == skl2) return false;
	return true; 
}

std::ostream & operator << (std::ostream & strm, const LZespolona & L){
	strm << "(" << L.get_re() << std::showpos << L.get_im() << std::noshowpos << "i)" ;
	return strm;
}

std::istream & operator >> (std::istream & strm, LZespolona & L){
	char temp;
	double liczb;
	strm >> temp;
	if(temp != '('){
		strm.setstate(std::ios::failbit);
	}
	strm >> liczb;
	L.set_re(liczb);
	strm >> liczb;
	L.set_im(liczb);
	strm >> temp;
	if(temp != 'i'){
		strm.setstate(std::ios::failbit);
	}
	strm >> temp;
	if(temp != ')'){
		strm.setstate(std::ios::failbit);
	}
	return strm;
}







