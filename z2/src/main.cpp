#include <iostream>
#include <fstream>
#include "BazaTestu.hh"
#include "WyrazenieZesp.hh"
#include "LZespolona.hh"
#include "Stats.hh"

int main(int argc, char **argv)
{

  if (argc < 2){
    std::cout << std::endl << " Brak opcji okreslajacej rodzaj testu." << std::endl << " Dopuszczalne nazwy to:  latwy, trudny." << std::endl << std::endl;
    return 1;
  }

  BazaTestu BazaT(argv[1]);

  if (BazaT.CzyZlapanoPlik() == false){
    std::cerr << " Inicjalizacja testu nie powiodla sie." << std::endl;
    return 1;
  }
  
  std::cout << std::endl << " Start testu arytmetyki zespolonej: " << argv[1] << std::endl << std::endl;

  WyrZesp WyrZ_Pyt;
  LZespolona odpU, odpS;
  Stats staty;
  int ilOdp;
  
  
  
  while (BazaT.pobierz_pytanie(WyrZ_Pyt)){//        Główna pętla programu 
    staty.dodajPyt();
    ilOdp = 0;
    while(ilOdp < 3){
    ilOdp++;
    	std::cout <<" Pytanie nr."<< staty.get_ilPyt() <<" " << WyrZ_Pyt << " = ";
    	std::cin >> odpU;
    	odpS = WyrZ_Pyt.Oblicz();
	    if(!std::cin.good()){
				std::cin.clear();
				std::cin.ignore(100,'\n');
				std::cout << std::endl << " Nie podano liczby zespolonej w odpowiedniej formie '(Re+Im*i)' pozostało :" << 3-ilOdp << " podejść" << std::endl;
			}
			else{
				ilOdp = 3;
				if(odpU == odpS ){
				std::cout << std::endl << " Zgadza się, podałeś poprawną odp." << std::endl;
				staty.dodajPopOdp();
				}
				else{
				std::cout << std::endl << " Odpowiedź jest błędna merytorycznie "<< std::endl;
				std::cout << " Poprawna Odp to: "<< odpS << std::endl;
				
				}
			}
			std::cout << std::endl;
		}			
  }

	BazaT.zakoncz_strm();
  std::cout << std::endl << " Koniec testu, ilość poprawnych odpowiedzi to: "<< staty.get_ilOdp() <<"/"<< staty.get_ilPyt() <<" ,wynik testu: "<<staty.procent() <<  "%";
	std::cout << std::endl ;

}
