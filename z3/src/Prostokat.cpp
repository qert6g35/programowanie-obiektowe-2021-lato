#include "Prostokat.hh"

Prostokat::Prostokat (Wektor<3> p1, Wektor<3> p2, Wektor<3> p3, Wektor<3> p4){
	Wektor<3> bok1 = p1 - p2,
	bok2 = p2 - p3,
	bok3 = p3 - p4, 
	bok4 = p4 - p1 ;
	if((abs(bok1.dlugosc() - bok3.dlugosc()) < eps)&&(abs(bok2.dlugosc() - bok4.dlugosc()) < eps) && ((bok1 * bok2) < eps) && ((bok3 * bok4) < eps)){
		punkty.push_back(p1);
		punkty.push_back(p2);
		punkty.push_back(p3);
		punkty.push_back(p4);
	}
	else{
		exit(1);
	}
}

/*
Prostokat Prostokat::trans(Wektor2D wek) const { // translacja
	Wektor2D p1, p2, p3, p4;
	p1 =	punkty[0] + wek;
	p2 =	punkty[1] + wek;
	p3 =	punkty[2] + wek;
	p4 =	punkty[3] + wek;
	Prostokat wyn(p1,p2,p3,p4);
	return wyn;
}

Prostokat Prostokat::rot(MacierzObr2x2 mac) const{ // rotacja 
	Wektor2D p1, p2, p3, p4;
	p1 =	mac * punkty[0];
	p2 =	mac * punkty[1];
	p3 =	mac * punkty[2];
	p4 =	mac * punkty[3];
	Prostokat wyn(p1,p2,p3,p4);
	return wyn;

}

const Wektor2D & Prostokat::operator [] (const int & ind) const{
	if((ind<4)&&(ind>-1)){
 		return punkty[ind];
 	}
 	else{
	 	std::cerr<<" Punkt Prostokąta poza zakresem"<<std::endl;
 	 	exit(1);
	}
}

std::ostream & operator << (std::ostream & strm, const Prostokat & F){
	strm<<"("<<F[0]<<","<<F[1]<<","<<F[2]<<","<<F[3]<<")";


	return strm;
}
*/
