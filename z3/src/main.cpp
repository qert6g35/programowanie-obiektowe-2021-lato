#include "Wektor2D.hh"
#include "Prostokat.hh"
#include <iostream>
#include <vector>
#include "Maciez2x2.hh"
#include "Dr2D_gnuplot_api.hh"
#include <cmath>


using drawNS::Point2D;
using drawNS::APIGnuPlot2D;

int main(){
	drawNS::Draw2DAPI * rys = new APIGnuPlot2D(-10,10,-10,10,0);
	Prostokat Org(Wektor2D(1.0,1.0), Wektor2D(1.0,3.0), Wektor2D(5.0,3.0), Wektor2D(5.0,1.0));
	Prostokat temp = Org;

	Wektor2D WekPrac;
	

	char mainwyb;
	int katobrotu,ilobr;
	double pomx,pomy;
	bool czyedyt = false;
	int idnowy , idstary;

idstary = temp.rysuj(rys);
	
	while(mainwyb != 'q'){
		std::cout<<std::endl<<"o - obróć prostokąt"<<std::endl<<"p - przesuń prostokąt"<<std::endl<<"w - wyświetl współrzędne prostokąta"<<std::endl<<"q - wyłącz program"<<std::endl<<"Co należy wykonać?  Opcja:";
		std::cin>>mainwyb;
		switch(mainwyb){
			case 'o':
				czyedyt = true;
				std::cout<<std::endl<<" Podaj kąt o jaki chcesz obrócić:";
				std::cin>>katobrotu;
				std::cout<<" Podaj ile razy chcesz obrócić o podany kąt:";
				std::cin>>ilobr;
				temp = temp.rot(MacierzObr2x2 (katobrotu*ilobr));
				idnowy = temp.rysuj(rys);
				//rys->erase_shape(idstary);
			break;
			case 'p':
				czyedyt = true;
				std::cout<<std::endl<<" Podaj wektor o jaki chcesz przesunąć"<<std::endl<<"   x:";
				std::cin>>pomx;
				std::cout<<"   y:";
				std::cin>>pomy;
				temp = temp.trans(Wektor2D(pomx,pomy));
				idnowy = temp.rysuj(rys);
				//rys->erase_shape(idstary);
			break;
			case 'w':
				std::cout<<std::endl<<" Współrzędne prostokąta którym operujemy to :"<<temp;
			break;
			case 'q':
				std::cout<<std::endl<<" Wyłączam program, ";
			break;
		}
		if(czyedyt){
		rys->erase_shape(idstary);
		idstary = idnowy;
		Org.wysDelRozm(temp);		
		czyedyt = false;
		}
	}

	delete rys;
	return 0;
}
/*

std::vector<point2D> dane_do_rysowania;
int id = pendzel -> draw_polygonial_chain(dane_do_rysowania, "red");

rysownik->change_color(id,yelow);// zmaiana koloru
rysownik->erase_shape(id); //usuwa figórę


*/
