#include "Wektor2D.hh"

const Wektor2D Wektor2D::operator + (const Wektor2D  & arg )const  {
	Wektor2D pom;
	pom[0] = xy[0] + arg[0];
	pom[1] = xy[1] + arg[1];

	return pom;
}
const Wektor2D Wektor2D::operator - (const Wektor2D  & arg )const  {
	Wektor2D pom;
	pom[0] = xy[0] - arg[0];
	pom[1] = xy[1] - arg[1];

	return pom;
}
	
const double Wektor2D::operator * (const Wektor2D  & arg )const {
	double pom;
	pom = xy[0] * arg[0] + xy[1] * arg[1];
	return pom;
}
 
 
const double & Wektor2D::operator [] (const int & ind) const{
 	if((ind<2)&&(ind>-1)){
 		return xy[ind];
 	 }
 	 else{
	 	std::cerr<<" Wektor poza zakresem"<<std::endl;
 	 	exit(1);
 	 }
 	}
 double & Wektor2D::operator [] (int ind) {
 	if((ind<2)&&(ind>=0)){
		return xy[ind];
	}
	else{
	 	std::cerr<<" Wektor poza zakresem"<<std::endl;
 	 	exit(1);
 	}
 }
 
 std::ostream & operator << (std::ostream & strm, const Wektor2D & W){
	strm << "[" << W[0] << "," << W[1] <<  "]" ;
	return strm;
}//

drawNS::Point2D konwertuj(Wektor2D wek){

drawNS::Point2D wyn(wek[0],wek[1]);
return wyn;

}
