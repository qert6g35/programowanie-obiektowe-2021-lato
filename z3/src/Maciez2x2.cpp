#include "Maciez2x2.hh"


const Wektor2D & MacierzObr2x2::operator [] (const int & ind) const{
	if((ind<2)&&(ind>-1)){
 		return wier[ind];
 	}
 		else{
	 	std::cerr<<" Wartość poza zakresem Macierzy"<<std::endl;
 	 	exit(1);
 	}
}

void MacierzObr2x2::set_kat(double arg){
	kat = arg;
	
	wier[0][0] = std::cos((kat*PI/180.0));
	wier[0][1] = std::sin((kat*PI/180.0))*(-1);
	
	wier[1][0] = std::sin((kat*PI/180.0));
	wier[1][1] = std::cos((kat*PI/180.0));
	
}
	
MacierzObr2x2::MacierzObr2x2(double _kat){
	Wektor2D pom;
	kat = _kat;
	
	pom[0] = std::cos((kat*PI/180.0));
	pom[1] = std::sin((kat*PI/180.0))*(-1);
	wier.push_back(pom);
	
	pom[0] = std::sin((kat*PI/180.0));
	pom[1] = std::cos((kat*PI/180.0));
	wier.push_back(pom);
}

const MacierzObr2x2 MacierzObr2x2::operator * (const MacierzObr2x2 & arg2) const{
	double pom;
	pom = ( arg2.get_kat() + kat);
	MacierzObr2x2 wyn(pom);
	return wyn;
}

const Wektor2D MacierzObr2x2::operator * (const Wektor2D & wektor) const{
Wektor2D wyn;
wyn[0] = wektor * wier[0];
wyn[1] = wektor * wier[1];
return wyn;
}


std::ostream & operator << (std::ostream & strm, const MacierzObr2x2 & M){
	strm << " MacierzObr o kącie:" << M.get_kat() ;
return strm;
}
