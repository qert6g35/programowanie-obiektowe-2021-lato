#ifndef WEKTOR2D_HH
#define WEKTOR2D_HH

#include <iostream>
#include <vector>
#include "Dr2D_gnuplot_api.hh"
#include <cmath>

class Wektor2D {
private:

std::vector<double> xy;

public:

	Wektor2D() {xy.push_back(0.0); xy.push_back(0.0);}
	Wektor2D(double _x, double _y) {xy.push_back(_x); xy.push_back(_y);}
	const double & operator [] (const int & ind) const ;//get
	double & operator [] (int  ind);//set
	const Wektor2D operator + (const Wektor2D  & arg ) const;
	const Wektor2D operator - (const Wektor2D  & arg )const ;
	const double operator * (const Wektor2D  & arg )const;// skalarny
	double dlugosc() const { return sqrt(pow(xy[0],2.0) + pow(xy[1],2.0)); }
	
 // można dodać mnożenie i dzielenie prez doubla ewentualnie 
};
drawNS::Point2D konwertuj(Wektor2D wek);

std::ostream & operator << (std::ostream & strm, const Wektor2D & W);

	//
#endif
