#ifndef MACIEZ2X2
#define MACIEZ2X2

#include <iostream>
#include <vector>
#include "Wektor2D.hh"
#include <cmath>
#include "Dr2D_gnuplot_api.hh"

const double PI = 3.141592653589793238462643383279502884197169399375105820974944592307;

class MacierzObr2x2 {
private:

	std::vector<Wektor2D> wier;
	double kat;//UWAGA to zawsze są stopnie, używamy albo "(kat*PI/180)" w funkcjach albo get rad jak potrzebujemy na zewnątrz radianów !!!!

public:
	double get_kat() const { return kat ;}
	double get_rad() const { return (kat*PI/180.0) ;}
	void set_kat(double arg);
	MacierzObr2x2(double kat);
	const Wektor2D & operator [] (const int & ind) const; // get, double b = Wek[0]
	
	const MacierzObr2x2 operator * (const MacierzObr2x2 & arg2) const; // zmiana kąta o Mobr + Mobrarg2
	const Wektor2D operator * (const Wektor2D & wektor) const;
};

std::ostream & operator << (std::ostream & strm, const MacierzObr2x2 & M);


#endif
