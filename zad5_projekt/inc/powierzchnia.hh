#ifndef POWIERZCHNIA_HH
#define POWIERZCHNIA_HH

#include "InterElem.hh"
#include "RysInter.hh"
#include "UkladWsp.hh"
#include <vector>


using drawNS::Point3D;

class powierzchnia: public UkladWsp , public RysInter ,public InterElem{
private:

	double wys;

public:

	powierzchnia(double Wys): wys(Wys) {nazwa_elementu = "podłoże";}
	void rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;
	void usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override { if(ID > -1)rysownik->erase_shape(ID);}
	bool czy_nad(double wys)override;
	bool czy_londowanie_dozwolone(Wektor<3> punkt_londowania)override;
};

#endif
