#ifndef DRON_HH
#define DRON_HH

#include <vector>
#include <array>
#include "RysInter.hh"
#include "UkladWsp.hh"
#include "gran6.hh"
#include "InterElem.hh"
#include "Prostopadloscian.hh"

using drawNS::Point3D;

   /*!
   * \file dron.hh
   *	File contains Class Dron.
   */
  
    /*!
   * \brief Dron
   * Class represents Dron in 3D environment.
   */
class Dron : public UkladWsp , public RysInter ,public InterElem{
private:
		/*!
     * \brief Cialo;
     * Prostopadloscian for Dron's body.
     */
	Prostopadloscian Cialo;
		/*!
     * \brief Wirniki;
     * vector<gran6> contains 4 Dron's rotors.
     */
	std::vector<gran6> Wirniki;
	
public:
    /*!
     * \brief Constructor
     * \param Srodek - srodek from class UkladWsp
     */
	Dron(const Wektor<3> & srodek_);
    /*!
     * \brief Constructor
     * \param bazaS - Cialo
     * \param bazaO - Cialo
     * \param gornaPodstCiala - Cialo
     * \param wysCialaWektor - Cialo
     * \param bazaO - r_wirnika
     * \param bazaO - wys_wirnika
     */
	Dron(const Wektor<3> & bazaS,const MacierzObr<3> & bazaO,const std::array<Wektor<3>, 4> & gornaPodstCiala,const  Wektor<3> & wysCialaWektor,const Wektor<3> & r_wirnika,const  Wektor<3> & wys_wirnika);
	    /*!
     * \brief rysuj responsible for drawing Dron in gnuplot 
     * \param rysownik comunication with gnuplot form RysInter
     */
	void rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;
	    /*!
     * \brief usun responsible for eraseing Dron from gnuplot 
     * \param rysownik comunication with gnuplot form RysInter
     */
	void usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;
		/*!
     * \brief translacjaW its make translation of every element in Dron
     * \param nowy is wektor for translarion
     */
	void translacjaW(Wektor<3> nowy);
			/*!
     * \brief rotacjaW its make rotation of every element in Dron
     * \param nowy is Matrix of rotation for every rotation
     */
	void rotacjaW(MacierzObr<3> nowa);
	    /*!
     * \brief Acess operator
     * \param ind - 0,1,2,3 gran6 from Wirniki.
     * \return value of gran6.
     */
	gran6 & operator [] (const int & ind);
	    /*!
     * \brief Acess operator
     * \param ind - 0,1,2,3 gran6 from Wirniki.
     * \return reference to  gran6.
     */
	gran6 & getWirnik(int ind);
			/*!
     * \brief czy_nad return 1 if flying object is above this one
     * \param wys is how high is flying the drone
     */
	bool czy_nad(double wys)override;
			/*!
     * \brief czy_londowanie_dozwolone return 1 if drone landing dont disturb that obj.
     * \param punkt_londowania point where done is going to land.
     */
  bool czy_londowanie_dozwolone(Wektor<3> punkt_londowania)override;
};

#endif
