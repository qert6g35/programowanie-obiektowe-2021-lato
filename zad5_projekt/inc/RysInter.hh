#ifndef RYSINTER_HH
#define RYSINTER_HH

#include "Dr3D_gnuplot_api.hh"

using drawNS::Point3D;
using drawNS::APIGnuPlot3D;


class RysInter{
protected:

	int ID = -10;

public:

	int get_id() {return ID;}
	virtual void rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik) = 0;
	virtual void usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik) = 0;
};


#endif
