#ifndef GRAN6_HH
#define GRAN6_HH

#include <vector>
#include "RysInter.hh"
#include "UkladWsp.hh"

const double Eps = 0.00001;

using drawNS::Point3D;

class gran6: public UkladWsp , public RysInter{
private:
	Wektor<3> UstWzgSr;
	std::vector<Wektor<3>> punkty;

public:

	gran6(const Wektor<3> & bazaS,const MacierzObr<3> & bazaO,const Wektor<3> & r,const Wektor<3> & wys);
	gran6();
	void rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;
	void usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;
	void translacjaUkladu(Wektor<3> UstWzgSrNEW);
	const Wektor<3> & operator [] (const int & ind) const;
	void rotacjaWlasna(MacierzObr<3> Obrot);
};


#endif
