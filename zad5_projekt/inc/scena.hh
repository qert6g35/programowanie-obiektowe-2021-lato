#ifndef SCENA_HH
#define SCENA_HH

#include <cmath>
#include <vector>
#include <array>
#include <iostream>
#include "dron.hh"
#include <thread>
#include <chrono>
#include "ObiektyTerenu.hh"
#include "powierzchnia.hh"

class scena{
private:
//	std::vector<Dron> Drony;
	std::vector<std::shared_ptr<Dron>> D;
	MacierzObr<3> VObrWir;
	double VDrona;
	double VObrDrona;
	std::vector<std::shared_ptr<InterElem>> elementy;
	std::vector<std::shared_ptr<RysInter>> elementy_rys;
public:
	scena(MacierzObr<3> VWir = MacierzObr<3>(57),double Vdrona = 2.0,double VOBRdrona = 1.0):VObrWir(VWir), VDrona(Vdrona), VObrDrona(VOBRdrona){}
	void setVDrona(double nowa) { VDrona = nowa;}
	void setVObrDrona(double nowa) { VObrDrona = nowa;}
	int Menu(int wyb);
	void Animacja_ruchu(int Nrdrona);
	void lec(const int & NrdronaElem,std::shared_ptr<drawNS::Draw3DAPI> rysownik);
	void dodajElement();
	void usunElement(std::shared_ptr<drawNS::Draw3DAPI> rys);
	void wyswListe();
	void rysujWszystkie(std::shared_ptr<drawNS::Draw3DAPI> rys);
	void progDodDrona(const double & X,const double & Y);
	void progDodWzgorze(const double & X,const double & Y);
	void progDodPowierzchnie(const double & H);
	void progDodPlaskowyrz(const double & X,const double & Y);
	bool czy_lec(Wektor<3> punkt_londowania,double wys);
};


#endif
/*



dodajDrona
usunDrona
sprawdzKolizje


*/
