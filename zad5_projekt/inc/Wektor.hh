#ifndef WEKTOR2D_HH
#define WEKTOR2D_HH

#include <initializer_list>
#include <iostream>
#include <vector>
#include <array>
#include <cmath>

//double eps = 0.000001;

using namespace std;

template <int ROZMIAR>
class Wektor { // ROZMIAR jest tu zmienną dla całej klasy

private:

	inline static int ile_wszystkich = 0;
	inline static int ile_teraz = 0;
	std::vector<double> xy;

public:
	~Wektor() {ile_teraz--; }
	Wektor<ROZMIAR>(std::initializer_list<double> il) : xy(il){ile_teraz++; ile_wszystkich++;}
	Wektor<ROZMIAR>();
	Wektor(const Wektor & nowy): xy(nowy.xy){ ile_teraz++; ile_wszystkich++;}
	const double & operator [] (const int & ind) const ;//get
	double & operator [] (int ind);//set
	const Wektor<ROZMIAR> operator + (Wektor<ROZMIAR> arg )const;
	const Wektor<ROZMIAR> operator - (Wektor<ROZMIAR> arg )const;
	const double operator * (const Wektor<ROZMIAR>  & arg )const ;// skalarny
	double dlugosc() const { if(ROZMIAR == 2)return sqrt(pow(xy[0],2.0) + pow(xy[1],2.0)); if(ROZMIAR == 3)return sqrt(pow(sqrt(pow(xy[0],2.0) + pow(xy[1],2.0)),2.0) + pow(xy[2],2.0)); return 0;}
	double get(int id);
	static int get_int_wszystkich() { return ile_wszystkich; }
	static int get_int_teraz() { return ile_teraz; }
};

template <int ROZMIAR>
std::ostream & operator << (std::ostream & strm, const Wektor<ROZMIAR> & W);



#endif
