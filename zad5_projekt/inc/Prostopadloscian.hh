#ifndef PROSTOPADLOSCIAN_HH
#define PROSTOPADLOSCIAN_HH

#include <vector>
#include <array>
#include "RysInter.hh"
#include "UkladWsp.hh"

using drawNS::Point3D;


class Prostopadloscian : public UkladWsp , public RysInter {
private:
	
	std::vector<Wektor<3>> punkty;
	
public:

	//Prostopadloscian(Wektor<3>  bazaS, MacierzObr<3>  bazaO, std::array<Wektor<3>, 4>  podst, Wektor<3>  wys);
	Prostopadloscian(const Wektor<3> & bazaS,const MacierzObr<3> & bazaO,const std::array<Wektor<3>, 4> & podst,const Wektor<3> & wys);
	Prostopadloscian();
	const Wektor<3> & operator [] (const int & ind) const;
	void rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;
	void usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;


};



#endif
