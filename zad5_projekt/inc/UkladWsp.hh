#ifndef UKLADWSP_HH
#define UKLADWSP_HH

#include "Wektor.hh"
#include "Maciez.hh"

class UkladWsp {// klasa rozszerzeniowa
protected:
	Wektor<3> srodek;
	MacierzObr<3> orient;

public:

	UkladWsp(Wektor<3> bazaS ,MacierzObr<3> bazaO): srodek(bazaS), orient(bazaO) {}
	UkladWsp(): srodek(Wektor<3>()), orient(MacierzObr<3>()) {}
	void rotacja(MacierzObr<3> nowa) {orient = orient * nowa;}
	void translacja(Wektor<3> nowy) {srodek = srodek + nowy;}
	MacierzObr<3> getOrient() {return orient;}
	Wektor<3> getSrodek() {return srodek;}
};


#endif
