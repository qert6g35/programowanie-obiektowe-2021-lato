#ifndef INTERELEM_HH
#define INTERELEM_HH

#include <string>
#include "Wektor.hh"
class InterElem{
protected:

std::string nazwa_elementu;
double PrzestrznOsobista;

public:

std::string get_nazwa(){return nazwa_elementu;}
 
virtual bool czy_nad(double wys) = 0;
virtual bool czy_londowanie_dozwolone(Wektor<3> punkt_londowania) = 0;
} ;


#endif
