#ifndef OBIEKTYTERENU_HH
#define OBIEKTYTERENU_HH


#include <random>
#include "RysInter.hh"
#include "UkladWsp.hh"
#include "InterElem.hh"
#include "Prostopadloscian.hh"

class plaskowyrz : public InterElem , public RysInter, public UkladWsp{//***********************************************plaskowyrz
private:
	std::vector<Wektor<3>> Podstawa;
	Wektor<3> WektorWys;

public:
	plaskowyrz(const Wektor<3> & Srodek);
	void rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;
	void usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;
	bool czy_nad(double wys)override;
  bool czy_londowanie_dozwolone(Wektor<3> punkt_londowania)override;
};

class wzgorze : public InterElem, public RysInter, public UkladWsp{//***********************************************wzgorze
private:
	std::vector<Wektor<3>> Podstawa;
	Wektor<3> Szczyt;
public:
	wzgorze(const Wektor<3> & Srodek);
  wzgorze(const double & wys,const double & szerokosc,const double & ilPktPodst,const Wektor<3> & Srodek);
	void rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;
	void usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;
	bool czy_nad(double wys)override;
  bool czy_londowanie_dozwolone(Wektor<3> punkt_londowania)override;
};

class plaskowyrzProst : public InterElem, public RysInter, public UkladWsp{//***********************************************plaskowyrz_prostopadloscienny
private:
	Prostopadloscian Obiekt;

public:
	plaskowyrzProst(const Wektor<3> & Srodek);
	void rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;
	void usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik)override;
	bool czy_nad(double wys)override;
  bool czy_londowanie_dozwolone(Wektor<3> punkt_londowania)override;
};


#endif
