#include "dron.hh"

Dron::Dron(const Wektor<3> & bazaS,const MacierzObr<3> & bazaO,const std::array<Wektor<3>, 4> & gornaPodstCiala,const  Wektor<3> & wysCialaWektor,const Wektor<3> & r_wirnika,const  Wektor<3> & wys_wirnika){
	Prostopadloscian temp(bazaS,bazaO,gornaPodstCiala,wysCialaWektor);
	Cialo = temp;
	gran6 tempWirnik(bazaS,bazaO,r_wirnika,wys_wirnika);
	tempWirnik.translacjaUkladu(gornaPodstCiala[0]);
	Wirniki.push_back(tempWirnik);
	
	tempWirnik = gran6(bazaS,bazaO,r_wirnika,wys_wirnika);
	tempWirnik.translacjaUkladu(gornaPodstCiala[1]);
	Wirniki.push_back(tempWirnik);
	
	tempWirnik = gran6(bazaS,bazaO,r_wirnika,wys_wirnika);
	tempWirnik.translacjaUkladu(gornaPodstCiala[2]);
	Wirniki.push_back(tempWirnik);
	
	tempWirnik = gran6(bazaS,bazaO,r_wirnika,wys_wirnika);
	tempWirnik.translacjaUkladu(gornaPodstCiala[3]);
	Wirniki.push_back(tempWirnik);

	nazwa_elementu = "dron";
}

Dron::Dron(const Wektor<3> & srodek_){
	//	Dron donPutas(srodek,orient,temp,wys,R,wys2);
	Wektor<3> Srodek({srodek_[0],srodek_[1],2.5});
	MacierzObr<3> orient(0);
	std::array<Wektor<3>,4> gornaPodstCiala = {Wektor<3>({5.0,7.5,2.5}),Wektor<3>({-5.0,7.5,2.5}),Wektor<3>({-5.0,-7.5,2.5}),Wektor<3>({5.0,-7.5,2.5})};
	Wektor<3> wysCialaWektor({0.0,0.0,-5});
	Wektor<3> r_wirnika({3.5,0.0,0.0});
	Wektor<3> wys_wirnika({0.0,0.0,2.5});
	PrzestrznOsobista = 0;
	srodek = srodek_;
	
	Prostopadloscian temp(Srodek,orient,gornaPodstCiala,wysCialaWektor);
	Cialo = temp;
	
	gran6 tempWirnik(Srodek,orient,r_wirnika,wys_wirnika);
	tempWirnik.translacjaUkladu(gornaPodstCiala[0]);
	Wirniki.push_back(tempWirnik);
	
	tempWirnik = gran6(Srodek,orient,r_wirnika,wys_wirnika);
	tempWirnik.translacjaUkladu(gornaPodstCiala[1]);
	Wirniki.push_back(tempWirnik);
	
	tempWirnik = gran6(Srodek,orient,r_wirnika,wys_wirnika);
	tempWirnik.translacjaUkladu(gornaPodstCiala[2]);
	Wirniki.push_back(tempWirnik);
	
	tempWirnik = gran6(Srodek,orient,r_wirnika,wys_wirnika);
	tempWirnik.translacjaUkladu(gornaPodstCiala[3]);
	Wirniki.push_back(tempWirnik);

	nazwa_elementu = "dron";

	Wektor<3> SrodekPom({srodek[0],srodek[1],0.0});
		Wektor<3> PunktPom;

	for(int i = 0; i<4 ;i++){
		for(int j = 0; j<12 ;j++){
			PunktPom = Wektor<3>({Wirniki[i][j][0],Wirniki[i][j][1],0.0});
			if(PrzestrznOsobista < (PunktPom-SrodekPom).dlugosc()){PrzestrznOsobista = (PunktPom-SrodekPom).dlugosc();}
			}		
		}
}


void Dron::rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik){
	Cialo.rysuj(rysownik);
	for(int i = 0 ; i < 4 ; i++)
	Wirniki[i].rysuj(rysownik);
}

void Dron::usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik){
std::cout<<std::endl<<" HAPPY CPP NOISES "<<std::endl;
}

void Dron::translacjaW(Wektor<3> nowy){
	this->translacja(nowy);
	Cialo.translacja(nowy);
	for(int i = 0; i < 4 ; i++){
		Wirniki[i].translacja(nowy);	
		
		
	}
}
void Dron::rotacjaW(MacierzObr<3> nowa){
	this->rotacja(nowa);
	Cialo.rotacja(nowa);
	for(int i = 0; i < 4 ; i++)
		Wirniki[i].rotacja(nowa);
	
}

gran6 & Dron::getWirnik(int ind){
if((ind<4)&&(ind>-1)){
 		return Wirniki[ind];
 	 }
 	 else{
	 	std::cerr<<" Wirnik poza zakresem"<<std::endl;
 	 	exit(1);
 	 }




}

gran6 & Dron::operator [] (const int & ind){
if((ind<4)&&(ind>-1)){
 		return Wirniki[ind];
 	 }
 	 else{
	 	std::cerr<<" Wirnik poza zakresem"<<std::endl;
 	 	exit(1);
 	 }




}

bool Dron::czy_nad(double wys){
	double wysObj = Wirniki[0][10][2]; 
	if(wysObj <= wys) return 1;
	return 0;	
}
bool Dron::czy_londowanie_dozwolone(Wektor<3> punkt_londowania){
	if((srodek-punkt_londowania).dlugosc() > PrzestrznOsobista  )return 1;
	return 0;
 
}






















