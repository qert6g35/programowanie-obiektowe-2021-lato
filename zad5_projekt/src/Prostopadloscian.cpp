#include "Prostopadloscian.hh"
	Prostopadloscian::Prostopadloscian(){
		Wektor<3> pom;
		MacierzObr<3> pomM(0);
		srodek = pom;
		orient = pomM;
		for(int i = 0; i < 8; i++)
		punkty.push_back(pom);	
	}

	Prostopadloscian::Prostopadloscian(const Wektor<3> & bazaS,const MacierzObr<3> & bazaO,const std::array<Wektor<3>, 4> & podst,const Wektor<3> & wys){
	double eps = 0.000001;

	srodek = bazaS;
	orient = bazaO;

	Wektor<3> temp;

	Wektor<3> bok1 = podst[0] - podst[1],
	bok2 = podst[1] - podst[2],
	bok3 = podst[2] - podst[3], 
	bok4 = podst[3] - podst[0] ;
	
	if((std::abs(bok1.dlugosc() - bok3.dlugosc()) < eps)&&(std::abs(bok2.dlugosc() - bok4.dlugosc()) < eps) && (std::abs(bok1 * bok2) < eps) && (std::abs(bok3 * bok4) < eps)&& (std::abs(bok1 * wys) < eps)&& (std::abs(bok2 * wys) < eps)){
	for(int i = 0; i < 4; i++)
		punkty.push_back(podst[i]);
	for(int i = 0; i < 4; i++){
		temp = podst[i] + wys;
		punkty.push_back(temp);
	}
	}
	else{
		std::cerr<<std::endl<<"	BŁĄD TWORZENIA PROSTOPADŁOŚCIANU "<<std::endl<<std::endl;
		exit(1);
	}
}

const Wektor<3> & Prostopadloscian::operator [] (const int & ind) const{
if((ind<8)&&(ind>-1)){
 		return punkty[ind];
 	 }
 	 else{
	 	std::cerr<<" Wektor poza zakresem"<<std::endl;
 	 	exit(1);
 	 }


}

void Prostopadloscian::rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik){
	this->usun(rysownik);
	std::vector<std::vector<Point3D>> wyn;
	std::vector<Point3D> push;
	Wektor<3> pom({0.0,0.0,0.0});
	for(int i = 0; i < 4; i++){
		pom = orient * punkty[i];
		push.push_back(Point3D(pom[0] + srodek[0],pom[1] + srodek[1],pom[2] + srodek[2]));
	}
	wyn.push_back(push);

	for(int i = 0; i < 4; i++){
		pom = orient * punkty[4+i];
		push[i] = Point3D(pom[0] + srodek[0],pom[1] + srodek[1],pom[2] + srodek[2]);
	}

	wyn.push_back(push);
	

	ID = rysownik->draw_polyhedron(wyn);
}

void Prostopadloscian::usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik){
	if(ID > -1)
		rysownik->erase_shape(ID);	
}
























