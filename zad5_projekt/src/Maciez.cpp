#include "Maciez.hh"

template <int ROZMIAR>
const Wektor<ROZMIAR> & MacierzObr<ROZMIAR>::operator [] (const int & ind) const{
	if((ind<ROZMIAR)&&(ind>-1)){
 		return wier[ind];
 	}
 		else{
	 	std::cerr<<" Wartość poza zakresem Macierzy"<<std::endl;
 	 	exit(1);
 	}
}

template <int ROZMIAR>
Wektor<ROZMIAR> & MacierzObr<ROZMIAR>::operator [] (int ind) {
	if((ind<ROZMIAR)&&(ind>-1)){
 		return wier[ind];
 	}
 		else{
	 	std::cerr<<" Wartość poza zakresem Macierzy"<<std::endl;
 	 	exit(1);
 	}
}

template <int ROZMIAR>
MacierzObr<ROZMIAR>::MacierzObr(){
	if(ROZMIAR == 2){
		Wektor<ROZMIAR> pom = {0.0 , 0.0};	
		pom[0] = 0;
		pom[1] = 0;
		wier.push_back(pom);		
		pom[0] = 0;
		pom[1] = 0;
		wier.push_back(pom);
	}
	if(ROZMIAR == 3){
		Wektor<ROZMIAR> pom = {0.0 , 0.0, 0.0};
		wier.push_back(pom);
		wier.push_back(pom);
		wier.push_back(pom);
		wier[0][0] = 1;
		wier[1][1] = 1;
		wier[2][2] = 1;
	}

}

template <int ROZMIAR>	
MacierzObr<ROZMIAR>::MacierzObr(double kat,OSKA os){
	if(ROZMIAR == 2){
		Wektor<ROZMIAR> pom = {0.0 , 0.0};
		
		pom[0] = std::cos((kat*PI/180.0));
		pom[1] = std::sin((kat*PI/180.0))*(-1);
		wier.push_back(pom);
		
		pom[0] = std::sin((kat*PI/180.0));
		pom[1] = std::cos((kat*PI/180.0));
		wier.push_back(pom);
	}
	else{
		if(ROZMIAR == 3){
			Wektor<ROZMIAR> pom = {0.0 , 0.0, 0.0};
			switch(os){
				case X:
					pom[0] = 1;
					pom[1] = 0;
					pom[2] = 0;
					wier.push_back(pom);
					pom[0] = 0;
					pom[1] = std::cos((kat*PI/180.0));
					pom[2] = std::sin((kat*PI/180.0))*(-1);
					wier.push_back(pom);
					pom[0] = 0;
					pom[1] = std::sin((kat*PI/180.0));
					pom[2] = std::cos((kat*PI/180.0));
					wier.push_back(pom);
				break;				
				case Y:
					pom[0] = std::cos((kat*PI/180.0));
					pom[1] = 0;
					pom[2] = std::sin((kat*PI/180.0));
					wier.push_back(pom);
					pom[0] = 0;
					pom[1] = 1;
					pom[2] = 0;
					wier.push_back(pom);
					pom[0] = std::sin((kat*PI/180.0))*(-1);
					pom[1] = 0;
					pom[2] = std::cos((kat*PI/180.0));
					wier.push_back(pom);				
				break;
				case Z:
					pom[0] = std::cos((kat*PI/180.0));
					pom[1] = std::sin((kat*PI/180.0))*(-1);
					pom[2] = 0;
					wier.push_back(pom);
					pom[0] = std::sin((kat*PI/180.0));
					pom[1] = std::cos((kat*PI/180.0));
					pom[2] = 0;
					wier.push_back(pom);
					pom[0] = 0;
					pom[1] = 0;
					pom[2] = 1;
					wier.push_back(pom);				
				break;
			}
			
	}
	else{
		std::cerr<<"Progoram nie obsługuje więcej nież 3 wymiarów obrotu"<<std::endl;
		exit(1);
		}
	}
}

template <int ROZMIAR>
void MacierzObr<ROZMIAR>::set_kat(double kat,OSKA os){
if(ROZMIAR == 2){
		
		wier[0][0] = std::cos((kat*PI/180.0));
		wier[0][1] = std::sin((kat*PI/180.0))*(-1);
		
		wier[1][0] = std::sin((kat*PI/180.0));
		wier[1][1] = std::cos((kat*PI/180.0));
	}
	else{
		switch(os){
			case X:
				wier[0][0] = 1;
				wier[0][1] = 0;
				wier[0][2] = 0;

				wier[1][0] = 0;
				wier[1][1] = std::cos((kat*PI/180.0));
				wier[1][2] = std::sin((kat*PI/180.0))*(-1);

				wier[2][0] = 0;
				wier[2][1] = std::sin((kat*PI/180.0));
				wier[2][2] = std::cos((kat*PI/180.0));
	
			break;				
			case Y:
				wier[0][0] = std::cos((kat*PI/180.0));
				wier[0][1] = 0;
				wier[0][2] = std::sin((kat*PI/180.0));

				wier[1][0] = 0;
				wier[1][1] = 1;
				wier[1][2] = 0;

				wier[2][0] = std::sin((kat*PI/180.0))*(-1);
				wier[2][1] = 0;
				wier[2][2] = std::cos((kat*PI/180.0));
				
			break;
			case Z:
				wier[0][0] = std::cos((kat*PI/180.0));
				wier[0][1] = std::sin((kat*PI/180.0))*(-1);
				wier[0][2] = 0;

				wier[1][0] = std::sin((kat*PI/180.0));
				wier[1][1] = std::cos((kat*PI/180.0));
				wier[1][2] = 0;

				wier[2][0] = 0;
				wier[2][1] = 0;
				wier[2][2] = 1;
				
			break;
			}	
	}
}

template <int ROZMIAR>
const MacierzObr<ROZMIAR> MacierzObr<ROZMIAR>::operator *(const MacierzObr<ROZMIAR> & arg) const{
	MacierzObr<ROZMIAR> wyn;
if(ROZMIAR == 2){
wyn[0][0] = arg[0][0]*wier[0][0] + arg[1][0]*wier[0][1];
wyn[0][1] = arg[0][1]*wier[0][0] + arg[1][1]*wier[0][1];
wyn[1][0] = arg[0][0]*wier[1][0] + arg[1][0]*wier[1][1];
wyn[1][1] = arg[0][1]*wier[1][0] + arg[1][1]*wier[1][1];
return wyn;
}
wyn[0][0] = arg[0][0]*wier[0][0] + arg[1][0]*wier[0][1] + arg[2][0]*wier[0][2];
wyn[0][1] = arg[0][1]*wier[0][0] + arg[1][1]*wier[0][1] + arg[2][1]*wier[0][2];
wyn[0][2] = arg[0][2]*wier[0][0] + arg[1][2]*wier[0][1] + arg[2][2]*wier[0][2];
wyn[1][0] = arg[0][0]*wier[1][0] + arg[1][0]*wier[1][1] + arg[2][0]*wier[1][2];
wyn[1][1] = arg[0][1]*wier[1][0] + arg[1][1]*wier[1][1] + arg[2][1]*wier[1][2];
wyn[1][2] = arg[0][2]*wier[1][0] + arg[1][2]*wier[1][1] + arg[2][2]*wier[1][2];
wyn[2][0] = arg[0][0]*wier[2][0] + arg[1][0]*wier[2][1] + arg[2][0]*wier[2][2];
wyn[2][1] = arg[0][1]*wier[2][0] + arg[1][1]*wier[2][1] + arg[2][1]*wier[2][2];
wyn[2][2] = arg[0][2]*wier[2][0] + arg[1][2]*wier[2][1] + arg[2][2]*wier[2][2];
return wyn;
}

template <int ROZMIAR>
const Wektor<ROZMIAR> MacierzObr<ROZMIAR>::operator * (const Wektor<ROZMIAR> & wektor) const{
	if(ROZMIAR == 2){
		Wektor<ROZMIAR> wyn;
		wyn[0] = wektor * wier[0];
		wyn[1] = wektor * wier[1];
		return wyn;
	}
		Wektor<ROZMIAR> wyn ;
		wyn[0] = wektor * wier[0];
		wyn[1] = wektor * wier[1];	
		wyn[2] = wektor * wier[2];
		return wyn;
}

template <int ROZMIAR>
std::ostream & operator << (std::ostream & strm, const MacierzObr<ROZMIAR> & M){
strm << std::endl;
	for(int y = 0; y < ROZMIAR ; y++){
	strm << "|";
		for(int x = 0; x < ROZMIAR ; x++){
			strm <<M [y][x];
			if(x<ROZMIAR-1) strm << ",";
		}
	strm<< "|" << std::endl;
	}
return strm;
}

template class MacierzObr<2>;
template class MacierzObr<3>;
template std::ostream & operator << (std::ostream & strm, const MacierzObr<2> & M);
template std::ostream & operator << (std::ostream & strm, const MacierzObr<3> & M);
