#include "gran6.hh"

gran6::gran6(){
	Wektor<3> pom;
	MacierzObr<3> pomM(0);
	srodek = pom;
	orient = pomM;
for(int i = 0; i < 12 ; i++)
punkty.push_back(pom);

}

gran6::gran6(const Wektor<3> & bazaS,const MacierzObr<3> & bazaO,const Wektor<3> & r,const Wektor<3> & wys){//R(R,0,-(Wys/2)) Wys(0,0,Wys)
	if(abs(r*wys) < Eps){
		srodek = bazaS;
		orient = bazaO;
		Wektor<3> pomr = r;
	
		MacierzObr<3> pom(60);
		
		for(int i = 0;i < 5 ; i++){
			punkty.push_back(pomr);
			pomr = pom * pomr;	
		}
		punkty.push_back(pomr);
		pomr = pomr + wys;
		pomr = pom * pomr;
		
		for(int i = 0;i < 5 ; i++){
			punkty.push_back(pomr);
			pomr = pom * pomr;	
		}
		punkty.push_back(pomr);
	}
	else{
	exit(2);
	}
}

const Wektor<3> & gran6::operator [] (const int & ind) const{
if((ind<12)&&(ind>-1)){
 		return punkty[ind];
 	 }
 	 else{
	 	std::cerr<<" Wektor poza zakresem"<<std::endl;
 	 	exit(1);
 	 }


}

void gran6::rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik){
	this->usun(rysownik);
	std::vector<std::vector<Point3D>> wyn;
	std::vector<Point3D> push;
	Wektor<3> pom({0.0,0.0,0.0});
	
	for(int i = 0; i < 6; i++){
		pom = (orient * ((punkty[i]) + UstWzgSr )) + srodek;
		push.push_back(Point3D(pom[0] ,pom[1] ,pom[2] ));
	}
	wyn.push_back(push);
	
	for(int i = 0; i < 6; i++){
		pom =( orient * ((punkty[i + 6]) + UstWzgSr )) + srodek;
		push[i] = Point3D(pom[0],pom[1],pom[2]);
	}
	wyn.push_back(push);
	

	ID = rysownik->draw_polyhedron(wyn);
}

void gran6::usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik){
	if(ID > -1)
		rysownik->erase_shape(ID)	;
}
void gran6::translacjaUkladu(Wektor<3> UstWzgSrNEW){
UstWzgSr = UstWzgSr +UstWzgSrNEW;
}

void gran6::rotacjaWlasna(MacierzObr<3> Obrot){
	for(int i = 0; i < 12;i++)
	punkty[i] = (Obrot * punkty[i]);
}




