#include "ObiektyTerenu.hh"

// _________________________________________plaskowyrzProstopadłościenny

plaskowyrzProst::plaskowyrzProst(const Wektor<3> & Srodek){

	std::random_device rand_dev;
  static std::mt19937 engine(rand_dev());
  std::uniform_real_distribution<double> ustawienie(0.0 , 360.0);
  std::uniform_real_distribution<double> wysokosc(5.0,35.0);
  std::uniform_real_distribution<double> dlugoscBoku(6.0,55.0);

	double X,Y,wys;
	X = dlugoscBoku(engine);
	Y = dlugoscBoku(engine);
	std::array<Wektor<3>, 4> pkt;

	pkt[0] = Wektor<3>({X/2.0,Y/2.0,0.0});
	pkt[1] = Wektor<3>({-X/2.0,Y/2.0,0.0});
	pkt[2] = Wektor<3>({-X/2.0,-Y/2.0,0.0});
	pkt[3] = Wektor<3>({X/2.0,-Y/2.0,0.0});
	
	wys = wysokosc(engine);
	srodek = Srodek;
//	Prostopadloscian(const Wektor<3> & bazaS,const MacierzObr<3> & bazaO,const std::array<Wektor<3>, 4> & podst,const Wektor<3> & wys);
	Prostopadloscian pom(Srodek, MacierzObr<3>(ustawienie(engine)), pkt, Wektor<3>({0.0,0.0,wys}));
	Obiekt = pom;	

}
void plaskowyrzProst::rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik){
	Obiekt.rysuj(rysownik);
}
void plaskowyrzProst::usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik){
	Obiekt.usun(rysownik);
}
bool plaskowyrzProst::czy_nad(double wys){
	for(int i = 0; i<8 ;i++){
		if(wys < Obiekt[i][2]) return 0;
	}
	return 1;
}
bool plaskowyrzProst::czy_londowanie_dozwolone(Wektor<3> punkt_londowania){return 1;}



// _________________________________________plaskowyrz

plaskowyrz::plaskowyrz(const Wektor<3> & Srodek){
	srodek = Srodek;
	std::random_device rand_dev;
  static std::mt19937 engine(rand_dev());
  
  std::uniform_real_distribution<> IloscWierz(3,16);
	double ilPktPodst = IloscWierz(engine);
	MacierzObr<3> pom(360.0/ilPktPodst);
	std::uniform_real_distribution<double> RWysokosc(6.0,26.0);
	std::uniform_real_distribution<double> OdlOdPodstawy(16.0,36.0);

	Wektor<3> pomr({0.0, 0.0 , 0.0});
	for(int i = 0;i < ilPktPodst ; i++){
		pomr = Wektor<3>({0.0, 0.0 , 0.0});
		pomr[0] = pomr[0] + OdlOdPodstawy(engine);
		if(pomr[0] > PrzestrznOsobista){
		PrzestrznOsobista = pomr[0];
		}
		for(int j = 0; j < i; j++){
			pomr = pom * pomr;
		}
		Podstawa.push_back(Wektor<3>({pomr[0] + Srodek[0],pomr[1] + Srodek[1],pomr[2] + Srodek[2]}));
	}
	WektorWys = Wektor<3>({0.0 ,0.0 , RWysokosc(engine)});

	nazwa_elementu = "płaskowyż";
}

void plaskowyrz::rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik){
	this->usun(rysownik);
	
	std::vector<std::vector<Point3D>> wyn;
	std::vector<Point3D> push;
	std::vector<Point3D> push2;
	
	int forPom = Podstawa.size();
	for(int i = 0; i < forPom; i++){
		push.push_back(Point3D(Podstawa[i][0] ,Podstawa[i][1] ,Podstawa[i][2] ));
	}
	wyn.push_back(push);

	for(int i = 0; i < forPom; i++){
		push2.push_back(Point3D(Podstawa[i][0]  ,Podstawa[i][1] ,Podstawa[i][2] + WektorWys[2]));
	}
	wyn.push_back(push2);
	

	ID = rysownik->draw_polyhedron(wyn);
}
void plaskowyrz::usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik){
	if(ID > -1)
		rysownik->erase_shape(ID)	;	
}
bool plaskowyrz::czy_nad(double wys){
	if(wys < WektorWys[2]) return 0;
	return 1;
}
bool plaskowyrz::czy_londowanie_dozwolone(Wektor<3> punkt_londowania){
	if((srodek-punkt_londowania).dlugosc() > PrzestrznOsobista  )return 1;
	return 0;
}


// _________________________________________Wzgorze

wzgorze::wzgorze(const Wektor<3> & Srodek){
	srodek = Srodek;
	
	std::random_device rand_dev;
  static std::mt19937 engine(rand_dev());
	
	std::uniform_real_distribution<> IloscWierz(3,16);
	double ilPktPodst = IloscWierz(engine);
	MacierzObr<3> pom(360.0/ilPktPodst);
	std::uniform_real_distribution<double> OdlOdPodstawy(6.0,26.0);
	PrzestrznOsobista = 0;
		
	for(int i = 0;i < ilPktPodst ; i++){
		Wektor<3> pomr({0.0, 0.0 , 0.0});
		pomr[0] = pomr[0] + OdlOdPodstawy(engine);
		if(pomr[0] > PrzestrznOsobista){
		PrzestrznOsobista = pomr[0];
		}
		for(int j = 0; j < i; j++){
			pomr = pom * pomr;
		}
		Podstawa.push_back(Wektor<3>({pomr[0] + Srodek[0],pomr[1] + Srodek[1],pomr[2] + Srodek[2]}));
	}
	std::uniform_real_distribution<double> wys(10.0,56.0);
	nazwa_elementu = "wzgórze";
	Szczyt = Wektor<3>({Srodek[0] ,Srodek[1] ,Srodek[2] + wys(engine)});
}

wzgorze::wzgorze(const double & wys,const double & szerokosc,const double & ilPktPodst,const Wektor<3> & Srodek){
	
	
	srodek = Srodek;
	MacierzObr<3> pom(360.0/ilPktPodst);
	Wektor<3> pomr({szerokosc / 2.0, 0.0 , 0.0});
	
	for(int i = 0;i < ilPktPodst +1 ; i++){
		Podstawa.push_back(Wektor<3>({pomr[0] + Srodek[0],pomr[1] + Srodek[1],pomr[2] + Srodek[2]}));
		pomr = pom * pomr;	
	}
	
	Szczyt = Wektor<3>({Srodek[0] ,Srodek[1] ,Srodek[2] + wys});
	nazwa_elementu = "wzgórze";
}

void wzgorze::rysuj(std::shared_ptr<drawNS::Draw3DAPI> rysownik){
	this->usun(rysownik);
	std::vector<std::vector<Point3D>> wyn;
	std::vector<Point3D> push;
	
	int forPom = Podstawa.size();
	for(int i = 0; i < forPom; i++){
		push.push_back(Point3D(Podstawa[i][0] ,Podstawa[i][1] ,Podstawa[i][2] ));
	}
	wyn.push_back(push);

	std::vector<Point3D> OddSczyt;	
	for(int i = 0; i < forPom; i++)
	OddSczyt.push_back(Point3D(Szczyt[0],Szczyt[1],Szczyt[2]));

	wyn.push_back(OddSczyt);
	

	ID = rysownik->draw_polyhedron(wyn);

}

void wzgorze::usun(std::shared_ptr<drawNS::Draw3DAPI> rysownik){
	if(ID > -1)
		rysownik->erase_shape(ID)	;
}
bool wzgorze::czy_nad(double wys){
	double wysObj = Szczyt[2] - srodek[2]; 
	if(wysObj <= wys) return 1;
	return 0;	
}
bool wzgorze::czy_londowanie_dozwolone(Wektor<3> punkt_londowania){
	if((srodek-punkt_londowania).dlugosc() > PrzestrznOsobista  )return 1;
	return 0;
}



