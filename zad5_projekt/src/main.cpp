#include <iostream>
#include <array>
#include <cmath>
#include <string>
#include "dron.hh"
#include "powierzchnia.hh"
#include "scena.hh"
#include "ObiektyTerenu.hh"

using drawNS::Point3D;
using drawNS::APIGnuPlot3D;


int main(int argc, char **argv){

std::shared_ptr<drawNS::Draw3DAPI> rys(new APIGnuPlot3D(-100,100,-100,100,-100,100,-1)); 
	//std::shared_ptr<drawNS::Draw3DAPI> api(new APIopenGL3D(-5,5,-5,5,-5,5,1000,&argc,argv));


	Wektor<3> srodek({0.0,0.0,0.2});
	MacierzObr<3> orient(0);
	std::array<Wektor<3>,4> temp = {Wektor<3>({5.0,7.5,2.5}),Wektor<3>({-5.0,7.5,2.5}),Wektor<3>({-5.0,-7.5,2.5}),Wektor<3>({5.0,-7.5,2.5})};
	Wektor<3> wys({0.0,0.0,-5});
	Wektor<3> R({3.5,0.0,0.0});
	Wektor<3> wys2({0.0,0.0,2.5});
	
	
	
	Dron donPutas(srodek);
	scena Scena;
	Scena.progDodDrona(0.0,0.0);
	Scena.progDodPowierzchnie(0.0);
	Scena.progDodWzgorze(74.0,35.0);
	Scena.progDodPlaskowyrz(72.0,40.0);
	Scena.progDodWzgorze(-65.0,20.0);
	Scena.progDodPlaskowyrz(-55.0,-40.0);
	Scena.progDodWzgorze(7.0,-80.0);

	//donPutas->rysuj(rys);
	rys->redraw();
	
 	int wyb,nr_drona;
  Scena.rysujWszystkie(rys);
 	while(wyb != 9){
 	wyb =	Scena.Menu(0);
 	switch(wyb){
 		case 1:
 		Scena.Menu(1);
 		std::cin>>nr_drona;
 		Scena.lec(nr_drona,rys);
 		break;
 		case 2:
 		Scena.Menu(2);
 		break;
 		case 3:
 		Scena.Menu(3);
 		default:
 		case 4:
 		Scena.Menu(4);
 		break;
 		case 5:
 		Scena.Menu(5);
 		break;
 		case 6:
 		Scena.Menu(6);
 		break;
 		case 7:
 		Scena.Menu(7);
 		Scena.usunElement(rys);
 		break;
 		}
 	//donPutas->rysuj(rys);
 	Scena.rysujWszystkie(rys);
	}
return 0;
}




/* BONUS
%%% WHY WE PROGRAM  IN C %%%

Gold : https://www.youtube.com/watch?v=tas0O586t80

Ariel listen to me
OO languages...
It's a mess.
Programing in C is better than anything they got over there.
The syntax might seem much sweeter
Where objects and subtypes play
But frills like inheritance 
will only get in the way!
Admire C's simple landscape 
Efficiently dangerous!
No templates or fancy pitfalls like Java and C++ !
Program in C
Program in C
Pointers, assembly, 
manage your memory 
with malloc() and free()!
Don't sink your app with runtime bloat 
Software in C will stay afloat
Do what you want there, close to the hardware!
Program in C

}*/
