#include "scena.hh"

void scena::Animacja_ruchu(int Nrdrona){
for(int i = 0; i < 4; i++)
D[Nrdrona]->getWirnik(i).rotacjaWlasna(VObrWir);
}

//std::this_thread::sleep_for(std::chrono::miliseconds(200));

bool scena::czy_lec(Wektor<3> punkt_londowania,double wys){

	int ilosc_elem = elementy.size();
	int czy_mozna = 1;

	for(int i = 0;i < ilosc_elem; i++){
	czy_mozna *= elementy[i]->czy_nad(wys) * elementy[i]->czy_londowanie_dozwolone(punkt_londowania);
	}
	if(!czy_mozna) return false;
	return true;
}


void scena::lec(const int & NrdronaElem,std::shared_ptr<drawNS::Draw3DAPI> rysownik){
	//!! !! !! !! !! !! !! !! !! !! !! !! !! !! !! !! !! !! setup
	int Nrdrona = 0;
	if(elementy[NrdronaElem-1]->get_nazwa() == "dron"){
	for(int i = 0;i < NrdronaElem-1; i++){
	if(elementy[i]->get_nazwa() == "dron") Nrdrona++;
	}

	// to wyżej to setup spujności tablic w scena !!!
	std::cout<<std::endl<<"	A następnie:"<<std::endl<<" Odległość na jaką ma się oddalić,"<<std::endl<<" Kąt o jaki ma się obrócić przed wyruszeniem w drogę,"<<std::endl<<" I wysokość na jaką ma się wzbić. "<<std::endl<<"double double double :"<<std::endl;

	double dlugosc, katobrotuWlewoOrg, wysokosc;
	std::cin>>dlugosc>>katobrotuWlewoOrg>>wysokosc;

	Wektor<3> pom = MacierzObr<3>(katobrotuWlewoOrg)*(D[Nrdrona]->getSrodek() + Wektor<3>({dlugosc,0.0,0.0}));

	if(this->czy_lec(pom ,wysokosc)){


	double Pom = wysokosc;
	while(Pom > 0){
		this->Animacja_ruchu(Nrdrona);
		if(Pom > VDrona){
			D[Nrdrona]->translacjaW(Wektor<3>({0.0,0.0,VDrona}));
		}
		else{
			D[Nrdrona]->translacjaW(Wektor<3>({0.0,0.0,Pom}));
		}
		D[Nrdrona]->rysuj(rysownik);
		rysownik->redraw();
		Pom = Pom - VDrona;
		std::this_thread::sleep_for(std::chrono::milliseconds(20));
	}
	
	double katobrotuWlewo = katobrotuWlewoOrg;
	while(katobrotuWlewo < 0.0){ katobrotuWlewo += 360.0;}
	while(katobrotuWlewo > 360.0){ katobrotuWlewo -= 360.0;}
	
	
	Pom = katobrotuWlewo;
	while(Pom > 0){ 
		this->Animacja_ruchu(Nrdrona);
		if(Pom > VObrDrona){
			D[Nrdrona]->rotacjaW(MacierzObr<3>(VObrDrona));
		}
		else{
			D[Nrdrona]->rotacjaW(MacierzObr<3>(Pom));
		}
		D[Nrdrona]->rysuj(rysownik);
		rysownik->redraw();
		Pom -= VObrDrona;
		std::this_thread::sleep_for(std::chrono::milliseconds(20));
	}
	
	Pom = dlugosc;
	while(Pom > 0){ 
		this->Animacja_ruchu(Nrdrona);
		if(Pom > VDrona){
			Wektor<3> doprzodu({0.0,(VDrona),0.0});
			D[Nrdrona]->translacjaW(D[Nrdrona]->getOrient() * doprzodu);
		}
		else{
			Wektor<3> doprzodu({0.0,Pom,0.0});
			D[Nrdrona]->translacjaW(D[Nrdrona]->getOrient() * doprzodu);
		}
		D[Nrdrona]->rysuj(rysownik);
		rysownik->redraw();
		Pom -= VDrona;
		std::this_thread::sleep_for(std::chrono::milliseconds(20));
	}
	
	Pom = -wysokosc;
	while(Pom < 0){
		this->Animacja_ruchu(Nrdrona);
		if(Pom < -VDrona){
			D[Nrdrona]->translacjaW(Wektor<3>({0.0,0.0, - VDrona}));
		}
		else{
			D[Nrdrona]->translacjaW(Wektor<3>({0.0,0.0,Pom}));
		}
		D[Nrdrona]->rysuj(rysownik);
		rysownik->redraw();
		Pom += VDrona;
		std::this_thread::sleep_for(std::chrono::milliseconds(20));
	}
}
else{
std::cerr<<std::endl<<" Dron zachaczy o przeszkodę ! "<<std::endl;
}
}
else{
std::cerr<<std::endl<<" to nie jest dron ! "<<std::endl;
}

}


void scena::progDodDrona(const double & X,const double & Y){
		std::shared_ptr<Dron> pom(new Dron({X,Y,0}));
		elementy.push_back(pom);
		elementy_rys.push_back(pom);
		D.push_back(pom);

}

void scena::progDodWzgorze(const double & X,const double & Y){
		std::shared_ptr<wzgorze> pom(new wzgorze({X,Y,0}));
		elementy.push_back(pom);
		elementy_rys.push_back(pom);
}

void scena::progDodPlaskowyrz(const double & X,const double & Y){
		std::shared_ptr<plaskowyrz> pom(new plaskowyrz({X,Y,0}));
		elementy.push_back(pom);
		elementy_rys.push_back(pom);
}

void scena::rysujWszystkie(std::shared_ptr<drawNS::Draw3DAPI> rys){
	int ilosc_elementow = elementy.size();
	for(int i = 0; i < ilosc_elementow ; i++){
		elementy_rys[i]->rysuj(rys);
	}
	rys->redraw();
	
}

void scena::dodajElement(){
	char pom;
	std::cout<<" Jaki element chcemy dodać?"<<std::endl<<" D - drona"<<std::endl<<" W - wzgórze  "<<std::endl<<" P - płaskowyrz  "<<std::endl<<" C - płaskowyrzprostopadłościenny  ";
	std::cin>>pom;
	std::shared_ptr<Dron> pomDron;
	std::shared_ptr<wzgorze> pomWzg;
	std::shared_ptr<plaskowyrz> pomPlas;
		std::shared_ptr<plaskowyrzProst> pomPlasP;
	double X, Y;
	switch(pom){
		case 'D':
		case 'd':
		std::cout<<" Podaj gdzie ma być ustawiony nowy dron:"<<std::endl<<" Przyjmujemy format x y (double) (double)"<<std::endl;
		std::cin>>X>>Y;
		pomDron = std::shared_ptr<Dron>(new Dron({X,Y,0}));
		elementy.push_back(pomDron);
		elementy_rys.push_back(pomDron);
		D.push_back(pomDron);
		/*	
		std::array<Wektor<3>,4> temp = {Wektor<3>({5.0,7.5,2.5}),Wektor<3>({-5.0,7.5,2.5}),Wektor<3>({-5.0,-7.5,2.5}),Wektor<3>({5.0,-7.5,2.5})};
		Dron * donPutas = new Dron(srodek,MacierzObr<3>(0),temp,Wektor<3>({0.0,0.0,-5}),Wektor<3>,Wektor<3>);
			std::vector<Dron> Drony;
			std::vector<InterElem> elementy_do_rys;
	std::vector<RysInter> elementy_do_rys;
	*/
		break;
		case 'W':
		case 'w':
		std::cout<<" Podaj gdzie ma być ustawione nowe wzgórze:"<<std::endl<<" Przyjmujemy format x y (double) (double)"<<std::endl;
		std::cin>>X>>Y;
		pomWzg = std::shared_ptr<wzgorze>(new wzgorze({X,Y,0}));
		elementy.push_back(pomWzg);
		elementy_rys.push_back(pomWzg);
		break;
		case 'P':
		case 'p':
		std::cout<<" Podaj gdzie ma być ustawiony nowy płaskowyrz:"<<std::endl<<" Przyjmujemy format x y (double) (double)"<<std::endl;
		std::cin>>X>>Y;
		pomPlas = std::shared_ptr<plaskowyrz>(new plaskowyrz({X,Y,0}));
		elementy.push_back(pomPlas);
		elementy_rys.push_back(pomPlas);
		break;
		case 'C':
		case 'c':
		std::cout<<" Podaj gdzie ma być ustawiony nowy płaskowyrz:"<<std::endl<<" Przyjmujemy format x y (double) (double)"<<std::endl;
		std::cin>>X>>Y;
		pomPlasP = std::shared_ptr<plaskowyrzProst>(new plaskowyrzProst({X,Y,0}));
		elementy.push_back(pomPlasP);
		elementy_rys.push_back(pomPlasP);
		break;
	
	}
}

void scena::usunElement(std::shared_ptr<drawNS::Draw3DAPI> rys){
	std::cout<<" Który emenet chcesz usunąć?";
	this->wyswListe();
	std::cout<<"podaj Nr(int): ";
	int pom;
	std::cin>>pom;
	if((pom > elementy.size())||(pom < 1)){
		std::cout<<" Podano niewłaściwy numer elementu!!";
	}
	else{
		int pomDoUsuwania = elementy.size();
		for(int i = pom ; i < pomDoUsuwania ; i++){
		elementy[i-1].swap(elementy[i]);
		elementy_rys[i-1].swap(elementy_rys[i]);
		}
		elementy.pop_back();
		rys->erase_shape(elementy_rys[pomDoUsuwania-1]->get_id());
		elementy_rys.pop_back();
	}
}

void scena::wyswListe(){
	int ilosc_elementow = elementy.size();
	std::cout<<std::endl;
	for(int i = 0;i < ilosc_elementow; i++){
	std::cout<<" Nr: "<<i+1<<" "<<elementy[i]->get_nazwa()<<std::endl;
	}
}

void scena::progDodPowierzchnie(const double & H){
	std::shared_ptr<powierzchnia> pom(new powierzchnia(H));
	elementy.push_back(pom);
	elementy_rys.push_back(pom);
}



int scena::Menu(int wyb){
	char pom;
	double pomD;
	switch(wyb){
		case 0:		
			std::cout<<std::endl<<" L - lot  na wybraną odległość "<<std::endl<<" Z - zmiana prędkości drona (bazowo(zalecane) prędkość lotu 2)"<<std::endl<<" O - zmiana prędkości obrotu drona (bazowo(zalecane) prędkość obrotu 1)"<<std::endl<<" W - wyświetlenie ilości wektorów w programie "<<std::endl<<" E - wyświetlenie listy elementów "<<std::endl<<" A - dodawanie elementu "<<std::endl<<" U - Usuwanie elementu "<<std::endl<<" Q - wyjście z programu"<<std::endl;
			std::cin>>pom;
			if(pom == 'L') return 1;
			if(pom == 'Z') return 2;
			if(pom == 'O') return 3;
			if(pom == 'W') return 4;
			if(pom == 'Q') return 9;
			if(pom == 'l') return 1;
			if(pom == 'z') return 2;
			if(pom == 'o') return 3;
			if(pom == 'w') return 4;
			if(pom == 'q') return 9;
			if(pom == 'E') return 5;
			if(pom == 'e') return 5;
			if(pom == 'a') return 6;
			if(pom == 'A') return 6;
			if(pom == 'U') return 7;
			if(pom == 'u') return 7;
			return  0;
		break;
		case 1:
			std::cout<<" Podaj który to dron:"<<std::endl;
			this->wyswListe();
			return 0;
		break;
		case 2:
			std::cout<<std::endl<<"Podaj nową prędkość lotu drona (double) :";
			std::cin>>pomD;
			std::cout<<std::endl;
			VDrona = std::abs(pomD)+0.0000001;
			return 0;
		break;
		case 3:
			std::cout<<std::endl<<"Podaj nową prędkość obrotu drona (double) :";
			std::cin>>pomD;
			std::cout<<std::endl;
			VObrDrona = std::abs(pomD)+0.0000001;
			return 0;
		break;
		case 4:
			std::cout<<std::endl<<"Obecnie jest :"<<Wektor<3>::get_int_teraz()<<" wektorów, od rozpoczęcia programu stworzono:"<<Wektor<3>::get_int_wszystkich()<<std::endl;
		break;
		case 5:
			this->wyswListe();
		break;
		case 6:
			this->dodajElement();
		break;
		case 7:
		break;
return 9;
}
}
