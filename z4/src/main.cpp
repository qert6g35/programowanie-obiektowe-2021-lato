#include "Wektor.hh"
#include "Prostopadloscian.hh"
#include "Prostokat.hh"
#include <iostream>
#include <array>
#include "Maciez.hh"
#include <cmath>
#include <initializer_list>
#include <string>

using drawNS::Point3D;
using drawNS::APIGnuPlot3D;

std::vector<std::vector<Point3D>> konwertuj(Prostopadloscian F){
	std::vector<std::vector<Point3D>> wyn;
	std::vector<Point3D> push;

	push.push_back(Point3D(F[0][0][0],F[0][0][1],F[0][0][2]));
	push.push_back(Point3D(F[0][1][0],F[0][1][1],F[0][1][2]));
	push.push_back(Point3D(F[0][2][0],F[0][2][1],F[0][2][2]));
	push.push_back(Point3D(F[0][3][0],F[0][3][1],F[0][3][2]));
	wyn.push_back(push);

	push[0] = Point3D(F[1][0][0],F[1][0][1],F[1][0][2]);
	push[1] = Point3D(F[1][1][0],F[1][1][1],F[1][1][2]);
	push[2] = Point3D(F[1][2][0],F[1][2][1],F[1][2][2]);
	push[3] = Point3D(F[1][3][0],F[1][3][1],F[1][3][2]);

	wyn.push_back(push);
	
	return wyn;
}

int rysuj(std::shared_ptr<drawNS::Draw3DAPI> rys,Prostopadloscian A){
return rys->draw_polyhedron(konwertuj(A));
}

int main(){

std::shared_ptr<drawNS::Draw3DAPI> rys(new APIGnuPlot3D(-10,10,-10,10,-10,10,0)); 

Prostokat podstawa(Wektor<3>({1.0,1.0,1.0}),Wektor<3>({1.0,3.0,1.0}),Wektor<3>({3.0,3.0,1.0}),Wektor<3>({3.0,1.0,1.0}));
Wektor<3> wys({0.0,0.0,4.0});
Prostopadloscian temp(podstawa , wys);
Prostopadloscian org(podstawa , wys);
rysuj(rys,org);

	char obrotyDecyzje;
	char mainwyb;//     done
	int ilobr;
	double pomx,pomy,pomz,katObrotu;
	bool czyedyt = false;// gotowe
	int idnowy , idstary;// gotowe
	MacierzObr<3> Mobrotu(0,X);
	MacierzObr<3> PoM(katObrotu,X);

while(mainwyb != 'q'){
		std::cout<<std::endl<<"o - obróć prostopadłościan"<<std::endl<<"p - przesuń prostopadłościan"<<std::endl<<"w - wyświetl współrzędne prostopadłościanu"<<std::endl<<"d - wyświetl długości boków prostopadłościanów "<<std::endl<<"m - wyświetl macierz ostatniego obrotu "<<std::endl<<"r - powtórz ostatnio wykonany obrót"<<std::endl<<"q - wyłącz program"<<std::endl<<"Co należy wykonać?  Opcja:";
		std::cin>>mainwyb;
		switch(mainwyb){
			case 'o':
				czyedyt = true;
				std::cout<<std::endl<<" Wpisujesz sekwencje obrotów, by zakończyć opcję podaj kropkę ( . ) "<<std::endl;
				while(obrotyDecyzje != '.'){
					std::cin>>obrotyDecyzje;
					switch(obrotyDecyzje){
							case 'X':
							case 'x':
							std::cin>>katObrotu;
							PoM.set_kat(katObrotu,X);
							Mobrotu = Mobrotu * PoM;		
							break;
							case 'Z':
							case 'z':
							std::cin>>katObrotu;
							PoM.set_kat(katObrotu,Z);
							Mobrotu = Mobrotu * PoM;
							break;
							case 'Y':
							case 'y':		
							std::cin>>katObrotu;
							PoM.set_kat(katObrotu,Y);
							Mobrotu = Mobrotu * PoM;
							break;
							case '.':
							break;						
							default:
							std::cerr<<std::endl<<" Podano niepoprawną oś obrotu (x/y/z)"<<std::endl;

							break;		
						}
					}
			std::cout<<std::endl<<" Ile razy powtorzyc sekwencje obrotow ? :";
			std::cin>>ilobr;
			for(int i = 0; i < ilobr; i++){
			temp = temp.rot(Mobrotu);
			}
			idnowy = rysuj(rys,temp);
			break;
			case 'p':
				czyedyt = true;
				std::cout<<std::endl<<" Podaj wektor o jaki chcesz przesunąć"<<std::endl<<"   x:";
				std::cin>>pomx;
				std::cout<<"   y:";
				std::cin>>pomy;
				std::cout<<"   z:";
				std::cin>>pomz;
				temp = temp.trans(Wektor<3>({pomx,pomy,pomz}));
				idnowy = rysuj(rys,temp);
				//rys->erase_shape(idstary);
			break;
			case 'm':
				std::cout<<std::endl<<" Macierz ostatnio wykonanego obrotu :"<<Mobrotu;
			break;
			case 'r':
				czyedyt = true;
				temp = temp.rot(Mobrotu);
				idnowy = rysuj(rys,temp);
			break;
			case 'w':
				std::cout<<std::endl<<" Współrzędne oryginalnego prostopadłościanu  to :"<<temp;		
				std::cout<<std::endl<<" Współrzędne prostopadłościanu którym operujemy to :"<<temp;
				std::cout<<std::endl;
			break;
			case 'd':
				std::cout<<std::endl<<" Wielkości oryginalnego prostopadłościanu  to :";
				org.dlugosci_bokow();			
				std::cout<<std::endl<<" Wielkości prostopadłościanu którym operujemy to :";
				temp.dlugosci_bokow();	
				std::cout<<std::endl;
			break;
			case 'q':
				std::cout<<std::endl<<" Wyłączam program, ";
			break;
		}
		if(czyedyt){
		rys->erase_shape(idstary);
		idstary = idnowy;
//		Org.wysDelRozm(temp);		
		czyedyt = false;
		}
	}
	



}//koniec maina

/*
%%% WHY WE PROGRAM  IN C %%%

Gold : https://www.youtube.com/watch?v=tas0O586t80

Ariel listen to me
OO languages?
It's a mess.
Programing in C is better than anything they got over there.
The syntax might seem much sweeter
Where objects and subtypes play
But frills like inheritance 
will only get in the way!
Admire C's simple landscape 
Efficiently dangerous!
No templates or fancy pitfalls like Java and C++ !
Program in C
Program in C
Pointers, assembly, 
manage your memory 
with malloc() and free()!
Don't sink your app with runtime bloat 
Software in C will stay afloat
Do what you want there, close to the hardware!
Program in C

}*/
