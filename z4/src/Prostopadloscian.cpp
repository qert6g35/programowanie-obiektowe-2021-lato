#include "Prostopadloscian.hh"

Prostopadloscian::Prostopadloscian (){
	Prostokat pusty;
	plaszczyzny.push_back(pusty);
	plaszczyzny.push_back(pusty);	
}

Prostopadloscian::Prostopadloscian (Prostokat p1,Wektor<3> Wys){
	Wektor<3> a = p1[0]-p1[1];
	Wektor<3> b = p1[1]-p1[2];
	Wektor<3> c = Wys ;	
	if((std::abs(a * b) < eps) && (std::abs(b * c) < eps) && (std::abs(c * a) < eps)){
		Prostokat p2 = p1.trans(Wys);
		plaszczyzny.push_back(p1);
		plaszczyzny.push_back(p2);
	}
	else{
		std::cerr<<std::endl<<"	BŁĄD TWORZENIA PROSTOPADŁOŚCIANU "<<std::endl<<std::endl;
		exit(1);
	}
}

Prostopadloscian::Prostopadloscian (Prostokat p1,Prostokat p2){
	Wektor<3> wys1 = p1[0] - p2[0];
	Wektor<3> wys2 = p1[1] - p2[1];
	Wektor<3> a1 = p1[0] - p1[1];
	Wektor<3> a2 = p1[1] - p1[2];
	Wektor<3> b1 = p2[0] - p2[1];
	Wektor<3> b2 = p2[1] - p2[2];
	if((std::abs(wys1.dlugosc()-wys2.dlugosc()) < eps) && (std::abs(a1 * wys1) < eps) && (std::abs(a2 * wys1) < eps) && (std::abs(b1 * wys1) < eps) && (std::abs(b2 * wys1) < eps)){
		plaszczyzny.push_back(p1);
		plaszczyzny.push_back(p2);
	}
	else{
		std::cerr<<std::endl<<"	BŁĄD TWORZENIA PROSTOPADŁOŚCIANU "<<std::endl<<std::endl;
		exit(1);
	}
}




const Prostokat & Prostopadloscian::operator [] (const int & ind) const{
	if((ind<2)&&(ind>-1)){
 		return plaszczyzny[ind];
 	}
 	else{
	 	std::cerr<<" Punkt Prostokąta poza zakresem"<<std::endl;
 	 	exit(1);
	}
}




Prostopadloscian Prostopadloscian::trans(Wektor<3> wek) const { // translacja
	Prostokat p1, p2;
	p1 =	plaszczyzny[0].trans(wek);
	p2 =	plaszczyzny[1].trans(wek);
	Prostopadloscian wyn(p1,p2);
	return wyn;
}



Prostopadloscian Prostopadloscian::rot(MacierzObr<3> mac) const{ // rotacja 
	Prostokat p1, p2;
	p1 =	plaszczyzny[0].rot(mac);
	p2 =	plaszczyzny[1].rot(mac);
	Prostopadloscian wyn(p1,p2);
	return wyn;

}


std::ostream & operator << (std::ostream & strm, const Prostopadloscian & F){
	strm<<"{"<<F[0]<<","<<F[1]<<"}";
	return strm;
}

	void Prostopadloscian::dlugosci_bokow ()const{
	double bokA, bokB, Wysokosc;
	bokA = std::abs((plaszczyzny[0][0] - plaszczyzny[0][1]).dlugosc());
	bokB = std::abs((plaszczyzny[0][0] - plaszczyzny[0][3]).dlugosc());
	Wysokosc = std::abs((plaszczyzny[0][0] - plaszczyzny[1][0]).dlugosc());
	std::cout<<" bok a:"<<bokA<<" bok b:"<<bokB<<" wysokosc:"<<Wysokosc;
	}

/*
	void Prostokat::wysDelRozm(const Prostokat temp) const{
	std::cout<<std::endl<<" Oryginalne rozmiany prostokąta:"<< (punkty[0] - punkty[1]).dlugosc() << " x " <<(punkty[1] - punkty[2]).dlugosc() << " x " <<(punkty[2] - punkty[3]).dlugosc() << " x " <<(punkty[3] - punkty[0]).dlugosc()<<std::endl;
	std::cout<<std::endl<<" Rozmiary prostokąta po edycjach:"<< (temp[0] - temp[1]).dlugosc() << " x " <<(temp[1] - temp[2]).dlugosc() << " x " <<(temp[2] - temp[3]).dlugosc() << " x " <<(temp[3] - temp[0]).dlugosc()<<std::endl;
	
	}
*/
