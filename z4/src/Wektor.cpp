#include "Wektor.hh"


template<int ROZMIAR>
Wektor<ROZMIAR>::Wektor(){
	for(int i = 0 ; i < ROZMIAR; i++){
	xy.push_back(0.0);
	}
}

template<int ROZMIAR>
double Wektor<ROZMIAR>::get ( int id ){
return xy[id];
}





template<int ROZMIAR>
const double & Wektor<ROZMIAR>::operator [] (const int & ind) const{
 	if((ind<ROZMIAR)&&(ind>-1)){
 		return xy[ind];
 	 }
 	 else{
	 	std::cerr<<" Wektor poza zakresem"<<std::endl;
 	 	exit(1);
 	 }
 	}
 
template<int ROZMIAR>
  double & Wektor<ROZMIAR>::operator [] (int ind) {
 	if((ind<ROZMIAR)&&(ind>=0)){
		return xy[ind];
	}
	else{
	 	std::cerr<<" Wektor poza zakresem"<<std::endl;
 	 	exit(1);
 	}
 }	



template<int ROZMIAR>
const Wektor<ROZMIAR> Wektor<ROZMIAR>::operator + (Wektor<ROZMIAR>  arg )const{
	for(int i = 0; i < ROZMIAR; i++)
	arg[i] = xy[i] + arg[i];
	return arg;
}




template<int ROZMIAR>
const Wektor<ROZMIAR> Wektor<ROZMIAR>::operator - (Wektor<ROZMIAR> arg )const  {
	for(int i = 0; i < ROZMIAR; i++)
	arg[i] = xy[i] - arg[i];
	return arg;
}

template<int ROZMIAR>
const double Wektor<ROZMIAR>::operator * (const Wektor<ROZMIAR>  & arg )const {
	double pom = 0;
	for(int i = 0; i < ROZMIAR; i++){
		pom = pom + xy[i] * arg[i];
	}
	return pom;
}

template<int ROZMIAR>
std::ostream & operator << (std::ostream & strm, const Wektor<ROZMIAR> & W){

	strm << "[" ;
	for(int i = 0; i < ROZMIAR; i++){
		if (i != 0) strm <<",";
		strm << W[i];
	}
	strm <<  "]" ;
	return strm;
}//



template class Wektor<2>;
template class Wektor<3>;
template class Wektor<4>;
template class Wektor<5>;
template class Wektor<6>;
template std::ostream & operator << (std::ostream & strm, const Wektor<2> & W);
template std::ostream & operator << (std::ostream & strm, const Wektor<3> & W);
template std::ostream & operator << (std::ostream & strm, const Wektor<4> & W);
template std::ostream & operator << (std::ostream & strm, const Wektor<5> & W);
template std::ostream & operator << (std::ostream & strm, const Wektor<6> & W);
