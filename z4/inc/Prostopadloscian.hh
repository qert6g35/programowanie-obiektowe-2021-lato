#ifndef PROSTOPADLOSCIAN_HH
#define PROSTOPADLOSCIAN_HH

#include "Wektor.hh"
#include "Prostokat.hh"
#include <iostream>
#include <vector>
#include "Maciez.hh"
#include "Dr3D_gnuplot_api.hh"

class Prostopadloscian {
private:
	std::vector<Prostokat> plaszczyzny;
public:
	Prostopadloscian rot(MacierzObr<3> mac) const;
	Prostopadloscian trans(Wektor<3> wek) const ;
	Prostopadloscian (Prostokat p1, Wektor<3> Wys); // Wys nie ma interpretacji punktu tylko wektora przesunięcia!!!!!
	Prostopadloscian (Prostokat p1,Prostokat p2);
	Prostopadloscian ();
	const Prostokat & operator [] (const int & ind) const;
	void dlugosci_bokow ()const;
};

std::ostream & operator << (std::ostream & strm, const Prostopadloscian & F);

#endif
