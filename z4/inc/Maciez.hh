#ifndef MACIEZ_HH
#define MACIEZ_HH

#include <iostream>
#include <vector>
#include "Wektor.hh"
#include <cmath>
#include "Dr3D_gnuplot_api.hh"

enum OSKA { X, Y, Z};

const double PI = 3.141592653589793238462643383279502884197169399375105820974944592307;

template <int ROZMIAR>
class MacierzObr {
private:

	std::vector<Wektor<ROZMIAR>> wier;
	Wektor<ROZMIAR> & operator [] (int ind);
	
public:
	void set_kat(double kat,OSKA os);
	MacierzObr<ROZMIAR>(double kat,OSKA _os = Z);
	MacierzObr<ROZMIAR>();
	const Wektor<ROZMIAR> & operator [] (const int & ind) const; // get, double b = Wek[0]
	
	const MacierzObr<ROZMIAR> operator *(const MacierzObr<ROZMIAR> & arg2) const; // zmiana kąta o Mobr + Mobrarg2
	const Wektor<ROZMIAR> operator * (const Wektor<ROZMIAR> & wektor) const;
};
template <int ROZMIAR>
std::ostream & operator << (std::ostream & strm, const MacierzObr<ROZMIAR> & M);


#endif
