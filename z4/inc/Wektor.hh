#ifndef WEKTOR2D_HH
#define WEKTOR2D_HH

#include <initializer_list>
#include <iostream>
#include <vector>
#include <array>
#include <cmath>

using namespace std;

template <int ROZMIAR>
class Wektor { // ROZMIAR jest tu zmienną dla całej klasy

private:

std::vector<double> xy;

public:
	Wektor<ROZMIAR>(std::initializer_list<double> il) : xy(il){}
	Wektor<ROZMIAR>();
	const double & operator [] (const int & ind) const ;//get
	double & operator [] (int ind);//set
	const Wektor<ROZMIAR> operator + (Wektor<ROZMIAR> arg )const;
	const Wektor<ROZMIAR> operator - (Wektor<ROZMIAR> arg )const;
	const double operator * (const Wektor<ROZMIAR>  & arg )const ;// skalarny
	double dlugosc() const { if(ROZMIAR == 2)return sqrt(pow(xy[0],2.0) + pow(xy[1],2.0)); if(ROZMIAR == 3)return sqrt(pow(sqrt(pow(xy[0],2.0) + pow(xy[1],2.0)),2.0) + pow(xy[2],2.0)); return 0;}
	double get(int id);
};

template <int ROZMIAR>
std::ostream & operator << (std::ostream & strm, const Wektor<ROZMIAR> & W);



#endif
