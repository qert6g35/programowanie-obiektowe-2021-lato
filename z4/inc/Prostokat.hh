#ifndef PROSTOKAT_HH
#define PROSTOKAT_HH

#include "Wektor.hh"
#include <iostream>
#include <vector>
#include "Maciez.hh"
#include "Dr3D_gnuplot_api.hh"

const double eps = 0.00001;

class Prostokat {
private:
	std::vector<Wektor<3>> punkty;
public:
	Prostokat rot(MacierzObr<3> mac) const;
	Prostokat trans(Wektor<3> wek) const;
	Prostokat (Wektor<3> p1, Wektor<3> p2, Wektor<3> p3, Wektor<3> p4);
	Prostokat ();
	const Wektor<3> & operator [] (const int & ind) const;//

};

std::ostream & operator << (std::ostream & strm, const Prostokat & F);

#endif
